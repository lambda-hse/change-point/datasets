#!/bin/bash
mkdir tcpd && cd tcpd
git clone https://github.com/alan-turing-institute/TCPD.git
mv TCPD/datasets/* . && rm -rf TCPD
cd tcpd
for d in * ; do
    if [ -e "$($d/get_$d.py)" ]; then
        cd $d/
        echo "Fetching $d"
        python get_$d.py
        cd ..
    fi
done